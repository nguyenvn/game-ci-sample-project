# [Unity/C#] Game.ci / Codemagic sample project

**Mina Pêcheux - May 2022**

![Codemagic build status](https://api.codemagic.io/apps/62860a2d178d24ec7e477c4d/unity-mac-workflow/status_badge.svg)

This demo project compares two automation solutions: [Game.ci](https://game.ci/) and [Codemagic](https://unitycicd.com/) and shows how we can mix them together to get faster builds.
